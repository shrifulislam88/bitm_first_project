<?php include("database.php"); ?>
<html>
	<head></head>
	<body>
		
		<?php
			$msg="";
			if(isset($_POST['submitbtn'])){
				$product_category=$_POST['product_category'];
				$product_name=$_POST['product_name'];
				$product_price=$_POST['product_price'];
				$product_availability=$_POST['product_availability'];
				$product_description=$_POST['product_description'];
				
				$success=insertProduct($product_category,$product_name,$product_price,$product_availability,$product_description);
				
				if($success){
					$msg="Inserted successfully";
				}
				else{
					$msg="Insertion failed";
				}
			}
			

		?>
		
		<?php 
			$category=getAllCategory();		
		?>
			
		<div>
			<div><?php echo $msg;?></div>
			<form method="post">
				<label>Product Category</label>
				<select name="product_category">
				<?php
					if($category){
						while($row=$category->fetch_assoc()){						
							$category_name=$row['category_name'];					
							echo "<option value='$category_name'>$category_name</option>";
						}
					}
					else{
						echo "<option value='default'>Default</option>";
					}
				?>
				</select>
				<br/>
			
				<label>Product Name</label>
				<input type="text" name="product_name"/>
				<br/>
				
				<label>Product Price</label>
				<input type="text" name="product_price"/>
				<br/>
				
				<label>Product Description</label>
				<textarea name="product_description"></textarea>
				<br/>
				
				<label>Product Availablity</label>
				<input type="radio" name="product_availability" value="1">Yes</input>
				<input type="radio" name="product_availability" value="0">No</input>
				<br/>
				
				<input type="submit" name="submitbtn" value="Add"/>
			</form>
		</div>
		
	</body>	
</html>