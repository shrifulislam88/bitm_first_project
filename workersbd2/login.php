<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>

<?php include("function.php"); ?>
<?php include("dbconnection.php"); ?>

<?php 

	$username="";
	$password="";
	$msg="";
	$success=false;
	if(isset($_POST['username']) && isset($_POST['password'])){
		$username=$_POST['username'];
		$password=$_POST['password'];

		if(username_password_check($username,$password)){
			$success=true;
			$_SESSION['username']=$username;
			$_SESSION['login']=1;
			redirect("index.php");
		}

		if(!$success){
			$msg="Username/Password didn't match";
		}
	}

?>

<body>
		<div>
			<h1>Admin Panel</h1>
			<form method="post">
			<table>
				<caption><?php echo $msg;?></caption>
				<tr>
					<td>Username</td>
					<td><input type="text" name="username" value="<?php echo $username; ?>"></input></td>
				</tr>
				<tr>
				<td>Password</td>
					<td><input type="password" name="password" value="<?php echo $password; ?>"></input></td>
				</tr>	
				<tr>
					<td></td>
					<td><input type="submit" value="Login"></input></td>
				</tr>
			</table>
			</form>
		</div>
</body>
</html>