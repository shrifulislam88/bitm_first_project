<?php
	
	function db_connection(){
		$servername = "localhost";
		$db_username = "root";
		$db_password = "";
		$db_name="product_m";

		$conn = mysqli_connect($servername, $db_username, $db_password,$db_name);
		
		if (!$conn) {
		    die("Connection failed: " . mysqli_connect_error());
		}		
		return $conn;
	}

	function db_close($conn){
		mysqli_close($conn);
	}


	

	function db_query($sql){
		$conn=db_connection();
		$result = $conn->query($sql);
		db_close($conn);
		
		if($result->num_rows>0){
			return $result;
		}		
		else{
			return false;			
		}
	}

	function db_query_insert($sql){
		$conn=db_connection();
		if($conn->query($sql)){
			db_close($conn);
			return true;
		}
		else{
			db_close($conn);
			return false;
		}
		
	}

	function execute_query($conn,$sql){
		$result=mysqli_query($conn,$sql);
		if($result){
			return $result;
		}
		else{
			return false;
		}
	}
	
	function getAllCategory(){
		$conn=db_connection();
		
		$sql="SELECT * FROM product_information";
		$result=execute_query($conn,$sql);
		
		return $result;
	}
	
	function insertProduct($product_category,$product_name,$product_price,$product_availability,$product_description){
		$conn=db_connection();
		$sql="INSERT into product_information(product_name,product_price,product_availability,product_description,product_category) values('$product_name','$product_price','$product_availability','$product_description','$product_category')";
		
		if(execute_query($conn,$sql))
		{
			return true;
		}
		else{
			return false;
		}
		
	}
	

	function username_password_check($user,$pass){
		$conn=db_connection();
		$sql = "SELECT username,password FROM authonication where username='$user' and password='$pass'";
		$result = $conn->query($sql);
			
		if ($result->num_rows > 0) {
			db_close($conn);
    		return true;
		}
		else
		{
			db_close($conn);
			return false;
		}
	}
	
	function getSearchResult($search){
		$conn=db_connection();
		$sql="SELECT * FROM product_information WHERE product_description like '%$search%' or product_name like '%$search%' or product_category like '%$search%'";
		$result=mysqli_query($conn,$sql);
		return $result;
	}
	
	function authentication_update($prev_username, $username,$password){
		$conn=db_connection();
		$sql="UPDATE authonication SET username='$username',password='$password' WHERE username='$prev_username'";
		
		$result=mysqli_query($conn,$sql);
		return $result;
	}
	
	function delete_account($username){
		$conn=db_connection();
		$sql="DELETE from authonication where username='$username'";
		//echo $sql;
		
		$result=mysqli_query($conn,$sql);
		return $result;
	}
?>
