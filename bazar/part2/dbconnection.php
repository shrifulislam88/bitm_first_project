<?php
	
	function db_connection(){
		$servername = "localhost";
		$db_username = "root";
		$db_password = "";
		$db_name="product_management";

		$conn = mysqli_connect($servername, $db_username, $db_password,$db_name);
		
		if (!$conn) {
		    die("Connection failed: " . mysqli_connect_error());
		}		
		return $conn;
	}

	function db_close($conn){
		mysqli_close($conn);
	}

	function db_query($sql){
		$conn=db_connection();
		$result = $conn->query($sql);
		db_close($conn);
		
		if($result->num_rows>0){
			return $result;
		}		
		else{
			return false;			
		}
	}

	function db_query_insert($sql){
		$conn=db_connection();
		if($conn->query($sql)){
			db_close($conn);
			return true;
		}
		else{
			db_close($conn);
			return false;
		}
		
	}

	function username_password_check($user,$pass){
		$conn=db_connection();
		$sql = "SELECT username,password FROM authentication where username='$user' and password='$pass'";
		$result = $conn->query($sql);
			
		if ($result->num_rows > 0) {
			db_close($conn);
    		return true;
		}
		else
		{
			db_close($conn);
			return false;
		}
	}
	
?>
