<?php session_start(); ?>
<?php include("dbconnection.php"); ?>
<?php include("functions.php"); ?>
<?php logincheck(); ?>
<!DOCTYPE html>
<html>
<head>
	<title>Home</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
		<div class="header">
			<div class="logo"><h1>Logo</h1></div>
			<div class="account">
				<div><?php echo $_SESSION['username']; ?></div>
				<div><a href="logout.php">logout</a></div>
			</div>
		</div>		
		<div class="middle">
			<div class="left_bar">
			<ul>
				<li><a href="home.php">Home</a></li>
				<li><a href="add_category.php">Add Category</a></li>
				<li><a href="add_product.php">Add Product</a></li>
				<li><a href="search_product.php">Search Product</a></li>
				<li><a href="edit_account.php">Edit Account info</a></li>
				<li><a href="create_account.php">Create account</a></li>
			</ul>			
			</div>
			<div class="right_bar">
				right side bar
			</div>

			<div class="main_content">
