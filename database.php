<?php
	function db_connection(){
		
		$db_host="localhost";
		$db_username="root";
		$db_password="";
		$db_name="product_management";
		
		$conn=mysqli_connect($db_host,$db_username,$db_password,$db_name);
		
		if(!$conn){		
			echo "Connection failed";
			die();
		}
		
		return $conn;
	}
	
	function db_close($conn){		
		mysqli_close($conn);
	}
	
	function execute_query($conn,$sql){
		$result=mysqli_query($conn,$sql);
		if($result){
			return $result;
		}
		else{
			return false;
		}
	}
	
	function getAllCategory(){
		$conn=db_connection();
		
		$sql="SELECT * FROM product_category";
		$result=execute_query($conn,$sql);
		
		return $result;
	}
	
	function insertProduct($product_category,$product_name,$product_price,$product_availability,$product_description){
		$conn=db_connection();
		$sql="INSERT into product_information(product_name,product_price,product_availability,product_description,product_category) values('$product_name','$product_price','$product_availability','$product_description','$product_category')";
		
		if(execute_query($conn,$sql))
		{
			return true;
		}
		else{
			return false;
		}
		
	}
	
	function userpasscheck($username,$password){
		$conn=db_connection();
		$sql="SELECT * FROM authenticaion WHERE username='$username' and password = '$password'";
		
		$result=$mysqli_query($conn,$sql);
		
		mysqli_close($conn);
		
		if($result->num_rows>0){
			return true;
		}
		else{
			return false;
		}
	}
	
	
	function getSearchResult($search){
		$conn=db_connection();
		$sql="SELECT * FROM product_information WHERE product_description like '%$search%' or product_name like '%$search%' or product_category like '%$search%'";
		$result=mysqli_query($conn,$sql);
		return $result;
	}
	
	function authentication_update($prev_username, $username,$password){
		$conn=db_connection();
		$sql="UPDATE authentication SET username='$username',password='$password' WHERE username='$prev_username'";
		
		$result=mysqli_query($conn,$sql);
		return $result;
	}
	
	function delete_account($username){
		$conn=db_connection();
		$sql="DELETE from authentication where username='$username'";
		//echo $sql;
		
		$result=mysqli_query($conn,$sql);
		return $result;
	}
?>